import os
import mimetypes
mimetypes.add_type("text/css", ".css", True)
class Config(object):
    SECRET_KEY = os.environ.get('SECRET_KEY') or 'you-will-never-guess'
