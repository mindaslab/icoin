"""importing app"""
from flask import render_template
from flask import send_from_directory
from flask import jsonify
import pandas as pd
from flask import request
from app import app
#from app.forms import LoginForm
#from flask import render_template, flash, redirect

#import numpy as np
#import datetime
#import os
import app.pandas_to_highcharts as tk
#from app.pandas_to_highcharts import *

TRADE_CSV = 'app/assets/data/bit_coin_prices_trade.csv'

@app.route('/')
@app.route('/index')
def index():
    """ rendering to index.html"""
    return render_template('index.html')

@app.route('/price')
def price():
    """price() gets price details from our source file and pass data to highcharts"""
    df_value = pd.read_csv(TRADE_CSV)
    data_as_array = tk.highchart_display_price_vs_time(df_value)
    return jsonify(data_as_array)


@app.route('/rolling_mean_highcharts')
def rollingmeanhighcharts():
    """ rollingmeanhighcharts() gets rolling mean datas from our source file and pass data to highcharts"""
    df_value = pd.read_csv(TRADE_CSV)
    df_value = df_value.fillna(0)
    rollingmean_as_array = tk.rolling_mean(df_value)
    return jsonify(rollingmean_as_array)


@app.route('/rsi_highcharts')
def rsi_highcharts():
    """rsi_highcharts() gets rsi datas from our source file and pass data to highcharts"""
    df_value = pd.read_csv(TRADE_CSV)
    df_value = df_value.fillna(0)
    rsi_as_array = tk.rsi(df_value)
    return jsonify(rsi_as_array)


@app.route('/buysell_pass_date')
def buysell_pass_date():
    """buysell_pass_date() gets buy or sell datas from  our source file  and pass data to highcharts"""
    date = request.args.get("data")
    df_value = pd.read_csv(TRADE_CSV)
    buysell_date_as_array = tk.buysell_date_pass_highcharts(df_value, date)
    return jsonify(buysell_date_as_array)


@app.route('/bolinger_upper_band')
def bolinger_upper_band():
    """def bolinger_upper_band() gets rsi bolinger upper band datas from  source file and pass data to highcharts"""
    df_value = pd.read_csv(TRADE_CSV)
    df_value = df_value.fillna(0)
    bolinger_upper_band_as_array = tk.bolinger_upper_band_highcharts(df_value)
    return jsonify(bolinger_upper_band_as_array)


@app.route('/bolinger_lower_band')
def bolinger_lower_band():
    """bolinger_lower_band() gets rsi bolinger lower band from our source file and pass data to highcharts"""
    df_value = pd.read_csv(TRADE_CSV)
    df_value = df_value.fillna(0)
    bolinger_lower_band_as_array = tk.bolinger_lower_band_highcharts(df_value)
    return jsonify(bolinger_lower_band_as_array)

@app.route('/assets/<path:path>')
def send_asset(path):
    """send_asset(path) denotes folder path of the source file"""
    return send_from_directory('assets', path)
	
@app.route('/ecargo')
def ecargo():
    """ rendering to check.html"""
    return render_template('ecargo.html')

@app.route('/sales')
def sales():
    """ rendering to check.html"""
    return render_template('check.html')
	
@app.route('/chat')
def chat():
    """ rendering to check.html"""
    return render_template('check.html')
	
@app.route('/image')
def image():
    """ rendering to check.html"""
    return render_template('check.html')		