$(document).ready(function(){
  $("button").click(function(){
  var routes=$('#route').val();
   var weights=$('#weight').val();
    var piece=$('#pieces').val();
	 var dates=$('#date_sub').val();
	 //alert(routes);
    var payload = {
      "route": routes,
      "weight": weights,
      "pieces": piece,
      "date": dates
    };
	 var params = {
      "route": routes,
      "date": dates
    };
	var params1 = {
      "route": routes
    };
	/* Price api functions*/
    $.ajax({
      method: "POST",
      url: "http://139.59.56.108:90/price",
      data: payload
    })
      .done(function( msg ) {
			$('#price').html(msg['price']);
			$('#up_price').html(msg['price_upper_limit']);
			$('#low_price').html(msg['price_lower_limit']);
			$('#rate_price').html(msg['rate_per_kg']);
			
			//creating the Gauge Meter
			var loprice=msg['price_lower_limit'];
			var rprice=msg['price'];
			var hiprice=msg['price_upper_limit'];
			var cal=(rprice-loprice)/2;
			var check1=loprice+cal;
			//alert(check1);
			var check2=rprice+cal;
			///alert(check2);			
			Highcharts.chart('price_cont', {
			chart: {
			type: 'gauge',
			plotBackgroundColor: null,
			plotBackgroundImage: null,
			plotBorderWidth: 0,
			margin:0
			},
			
			title: {
			text: ''
			},
			
			pane: {
			startAngle: -150,
			endAngle: 150,
			},
			exporting: {
			enabled: false
			},		
			// the value axis
			yAxis: {
			min: loprice,
			max: hiprice,
			title: {
			text: 'Price'
			},
			plotBands: [{
				from: loprice,
				to: check1,
				color: '#FF4500' // orange
			}, {
				from: check1,
				to: rprice,
				color: '#DDDF0D' // yellow
			}, {
				from: rprice,
				to: check2,
				color:'#55BF3B' // green
			},
			 {
				from: check2,
				to: hiprice,
				color: '#FF4500' // yellow
			}]			
			},
			series: [{
			name: 'Price',
			data: [rprice],
			tooltip: {
			valueSuffix: 'Price'
			}
			}]
			
			});      
		});

	  /* chart for new api with trending functions*/
		$.ajax({
			method: "POST",
			url: "http://139.59.56.108:90/predict",
			data: params
		})
      .done(function( msg ) {
						//data_ranges = msg['ranges'],
						data_averages = msg['averages'];
						//console.log(data_averages);
						
						Highcharts.chart('container', {
						
							title: {
								text: 'Past and future predication of Prices'
							},
						
							xAxis: {
								type: 'datetime'
							},
						
							yAxis: {
								title: {
									text: null
								}
							},
							exporting: {
							enabled: false
							},		
							tooltip: {
								crosshairs: true,
								shared: true,
								valueSuffix: ''
							},
						
							legend: {
							},
						
							series: [{
								name: 'Price',
								data: data_averages,
								zIndex: 1,
								marker: {
									fillColor: 'white',
									lineWidth: 2,
									lineColor: Highcharts.getOptions().colors[0]
								}
							}]
						});			
	  });
	  	/* box plotter chart api for week*/
		$.ajax({
			method: "POST",
			url: "http://139.59.56.108:90/weekchart",
			data: params1
		})
		.done(function( msg ) {
			data_labell = msg['categories'],
			data_averages = msg['data'];
			Highcharts.chart('week-chart', {

				chart: {
					type: 'boxplot'
				},
			
				title: {
					text: 'Week Chart'
				},
			
				legend: {
					enabled: false
				},
				exporting: {
				enabled: false
				},		
				xAxis: {
					categories: data_labell,
					title: {
						text: 'Days'
					}
				},
			
				yAxis: {
					title: {
						text: 'Price'
					}
				},
			
				series: [{
					name: 'Price',
					data: data_averages,
					tooltip: {
						headerFormat: 'Days<br/>'
					}
				}]
			
			});
			
		});
	  	/* box plotter chart api for month*/
		$.ajax({
			method: "POST",
			url: "http://139.59.56.108:90/monthchart",
			data: params1
		})
		.done(function( msg ) {
			//data_ranges = msg['ranges'],
			data_labell = msg['categories'],
			data_averages = msg['data'];
			Highcharts.chart('month-chart', {

				chart: {
					type: 'boxplot'
				},
			
				title: {
					text: 'Month Chart'
				},
			
				legend: {
					enabled: false
				},
				exporting: {
				enabled: false
				},		
				xAxis: {
					categories: data_labell,
					title: {
						text: 'Months'
					}
				},
			
				yAxis: {
					title: {
						text: 'Price'
					}
				},
			
				series: [{
					name: 'Price',
					data: data_averages,
					tooltip: {
						headerFormat: 'Days<br/>'
					}
				}]
			
			});
		});
  });

});