function tickPositioner(min, max) {
  var ticks = [],
  		interval = Highcharts.correctFloat(max - min) / 10;

  while (min <= max) {
    ticks.push(min);
    min = Highcharts.correctFloat(min + interval);
  }

  return ticks;
}
var chart;
$(document).ready(function() {

        $.getJSON('/price', function (data1) {
        $.getJSON('/rolling_mean_highcharts', function (data2) {
        $.getJSON('/rsi_highcharts', function (data3) {
        $.getJSON('/bolinger_upper_band', function (data4) {
        $.getJSON('/bolinger_lower_band', function (data5) {


         chart =  Highcharts.stockChart('chart-container', {

          exporting: {
				  buttons: {
					contextButton: {
					  selected: 4,
					  enabled: false
					},
					toggle: {
					  selected: 1,
					  text: 'Select range',
					  align: 'cenetr',
					  height: 30,
				
					  y: 6,
					  theme: {
						'stroke-width': 0.5,
						stroke: '#000000',
						r: 2
					  },
					  menuItems: [{
						type: 'day',
						count: 1,
						text: '3d',
						selected:true,
						onclick: function() {
						  this.rangeSelector.clickButton(0, true);
						}
					  }, {
						type: 'week',
						count: 1,
						text: '1w',
						onclick: function() {
						  this.rangeSelector.clickButton(1, true);
						}
					  }, {
						type: 'day',
						count: 15,
						text: '15 days',
						onclick: function() {
						  this.rangeSelector.clickButton(2, true);
						}
					  }, {
						type: 'month',
						count: 1,
						text: '1m',
						onclick: function() {
						  this.rangeSelector.clickButton(3, true);
						}
					  }, {
						type: 'month',
						count: 3,
						text: '3m',
						onclick: function() {
						  this.rangeSelector.clickButton(4, true);
						}
					  }, {
						type: 'all',
						text: 'All',
						onclick: function() {
						  this.rangeSelector.clickButton(5, true);
						}
					  }]
					}
			  }
			},
            legend: {
            enabled: true
            },
			title: {
				text: 'Bitcoin Price'
			},
			yAxis: [
            {
            lineWidth: 1,
            height: '80%',
            tickPositioner1: function () {
            var positions = [],

            tick = Math.floor(this.dataMin),
            increment = Math.ceil((this.dataMax - this.dataMin) / 4);

            if (this.dataMax !== null && this.dataMin !== null) {
            for (tick; tick - increment <= this.dataMax; tick += increment) {
            positions.push(tick);
            }
            }
            return positions;
            },


            },
            {

            height: '20%',
           top: '80%',
           tickInterval: 20,
            labels: {
            formatter: function () {
            return Highcharts.numberFormat(this.value,0);
            }
            },


            },

            ],
            plotOptions: {
               series: {
                   cursor: 'pointer',
                   point: {
                       events: {
                           click: function (e) {
                          function setDateZero(date){
                          return date < 10 ? '0' + date : date;
                          }
                             var date = new Date(this.category);
                              var curr_date = date.getDate();
                              var curr_month = date.getMonth();
                              var curr_year = date.getFullYear();
                              var months = curr_year+"-"+setDateZero(curr_month)+"-"+setDateZero(curr_date);

                            updatedate(months)
                            function updatedate(months)
                              {
                                $.getJSON('/buysell_pass_date', function (data6) {
                            $.ajax({
                            url: "buysell_pass_date",
                            type: "GET",
                            data: ({'data' : months,}),

                            success: function(data){
                              if(data[0].rolling_mean_buy	==true)
                              {
                              $('#rolling_mean').html("<label class='label label-success'>Buy</label>");
                              }
                              else if(data[0].rolling_mean_sell	==true){
                              $('#rolling_mean').html("<label class='label label-danger'>Sell</label>");
                              }
                              else{
                                $('#rolling_mean').html("<label class='label label-warning'>Wait</label>");
                              }

                              if (data[0].rsi_buy ==true)
                              {
                              $('#rsi').html("<label class='label label-success'>Buy</label>");
                              }
                              else if (data[0].rsi_sell ==true){
                              $('#rsi').html("<label class='label label-danger'>Sell</label>");
                              }
                              else{
                              $('#rsi').html("<label class='label label-warning'>Wait</label>");
                              }
                              if (data[0].macd_buy	==true)
                              {
                              $('#macd').html("<label class='label label-success'>Buy</label>");
                              }
                              else if (data[0].macd_sell	==true){
                              $('#macd').html("<label class='label label-danger'>Sell</label>");
                              }
                              else{
                              $('#macd').html("<label class='label label-warning'>Wait</label>");
                              }
                            if (data[0].bolinger_buy ==true)
                            {
                            $('#bolinger').html("<label class='label label-success'>Buy</label>");
                            }
                            else if (data[0].bolinger_sell ==true){
                            $('#bolinger').html("<label class='label label-danger'>Sell</label>");
                            }
                            else {
                            $('#bolinger').html("<label class='label label-warning'>Wait</label>");
                            }

                            }
                            });
                            });
                          }

                           }
                       }
                   }
               }
           },





            series: [
            {
            name: "Price in USD",
            data: data1,
            showInLegend: false

            },
            {
            showInLegend: false,
            name: "rolling Mean",
            data: data2,
            visible:false
            },
            {
            showInLegend: false,
            name: 'RSI',
            type:'rsi',
            data: data3,
            yAxis: 1,
            visible:false

            },
            {
            showInLegend: false,
            name: "Bolinger upper Band",
            data: data4,
            visible:false
            },
            {
            showInLegend: false, 
            name: "Bolinger Lower Band",
            data: data5,
            visible:false
          }
        /*  {
          type: 'pie',
          name: 'Total consumption',
          data: [{
              name: 'Jane',
              y: 13
          }, {
              name: 'John',
              y: 23
          }, {
              name: 'Joe',
              y: 19
          }],
          center: [200, 100],
          size: 100,
          showInLegend: false,
          dataLabels: {
              enabled: false
          }
        }*/


        ],

            });


            });
          });

      });

            });

});


$(".dropdown-menu button").click(function(){
  var selec = $(this).parents(".dropdown").find('.btn').html($(this).text() + ' <span class="caret"></span>');
  var selec = $(this).parents(".dropdown").find('.btn').val($(this).data('value'));
  var selected_value = $(this).data('value')
  show_hide_series(selected_value);
  if(selected_value == 3){
    var series = chart.series[3];
    var series1 = chart.series[4];
    if(series.visible){
      series.show();
      series1.show();
    }
    else{
      bolinger_band_show_hide();
    }
    if(series1.visible){
      series.show();
      series1.show();
    }
    else{
      bolinger_band_show_hide();
    }


  }


});
function show_hide_series(series_number)
{
  var series = chart.series[series_number];
  var visiblity = ! series.visible;
  series.setVisible(visiblity);


}
function bolinger_band_show_hide(){

 var series = chart.series[3];
 var series1 = chart.series[4];
 //series value
  series.hide();
  series1.hide();


}

});
