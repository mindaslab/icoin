import pandas as pd
import numpy as np

# highchart_display_price_vs_time convert datetime and return the dataframe values to list
def highchart_display_price_vs_time(dataframe):
    df = dataframe[["snapped_at", "price"]]
    df['snapped_at'] = pd.to_datetime(df['snapped_at']) # convert to datetime type
    df['snapped_at'] = df['snapped_at'].astype(np.int64) / 1e6 # convert to format needed for highcharts
    return df.values.tolist()

# rolling_mean convert datetime and return the dataframe values to list
def rolling_mean(dataframe):
    df = dataframe[["snapped_at", "rolling_mean"]]
    df['snapped_at'] = pd.to_datetime(df['snapped_at']) # convert to datetime type
    df['snapped_at'] = df['snapped_at'].astype(np.int64) / 1e6# convert to format needed for highcharts
    return df.values.tolist()

# rsi convert datetime and return the dataframe values to list
def rsi(dataframe):
    df = dataframe[["snapped_at", "rsi"]]
    df['snapped_at'] = pd.to_datetime(df['snapped_at']) # convert to datetime type
    df['snapped_at'] = df['snapped_at'].astype(np.int64) / 1e6# convert to format needed for highcharts
    return df.values.tolist()

# rsi_buysell_highcharts  convert datetime and return the dataframe values to list
def rsi_buysell_highcharts(dataframe):
    df = dataframe[["snapped_at",  "price","rolling_mean","rsi","rolling_mean_buy","rolling_mean_sell"]]
    df['snapped_at'] = pd.to_datetime(df['snapped_at']) # convert to datetime type
    df['snapped_at'] = df['snapped_at'].astype(np.int64) / 1e6# convert to format needed for highcharts
    return df.values.tolist()

# buysell_date_highcharts  convert datetime and return the dataframe values to dict
def buysell_date_highcharts(dataframe):
    df = dataframe[["snapped_at",  "price","rolling_mean","rolling_mean_buy","rolling_mean_sell","rsi","rsi_buy","rsi_sell","macd_buy","macd_sell","bolinger_buy","bolinger_buy"]]
    df['snapped_at'] = pd.to_datetime(df['snapped_at']) # convert to datetime type
    df['snapped_at'] = df['snapped_at'].astype(np.int64) / 1e6# convert to format needed for highcharts
    return df.to_dict()

# dataframe_to_dictionary_array  removes the index from the dataframe
def dataframe_to_dictionary_array(dataframe):
    columns = dataframe.columns
    values = dataframe.values
    array = []
    for row in values:
        dictionary = {}
        count = 0
        for column in columns:
            dictionary[column] = row[count]
            count += 1
        array.append(dictionary)
    return array

def buysell_date_pass_highcharts(dataframe, date):
    row = dataframe[dataframe["snapped_at"] == date]
    dataframe = dataframe_to_dictionary_array(row)
    return dataframe


#  bolinger_upper_band_highcharts  convert datetime and return the dataframe values to list
def bolinger_upper_band_highcharts(dataframe):
    df = dataframe[["snapped_at", "bollinger_upper"]]
    df['snapped_at'] = pd.to_datetime(df['snapped_at']) # convert to datetime type
    df['snapped_at'] = df['snapped_at'].astype(np.int64) / 1e6# convert to format needed for highcharts
    return df.values.tolist()

# bolinger_lower_band_highcharts  convert datetime and return the dataframe values to list
def bolinger_lower_band_highcharts(dataframe):
    df = dataframe[["snapped_at", "bollinger_lower"]]
    df['snapped_at'] = pd.to_datetime(df['snapped_at']) # convert to datetime type
    df['snapped_at'] = df['snapped_at'].astype(np.int64) / 1e6# convert to format needed for highcharts
    return df.values.tolist()
