# iCoin Stock Market Analyzer

![](icoin.png)

## Running This Software

1. Have Python Installed

2. Install the necessary packages

```
$ pip install -r requirements.txt
```

3. Run icoin

```
$ python icoin.py
```

4. Open http://127.0.0.1:5000/